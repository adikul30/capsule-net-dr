

```python
!nvidia-smi
```

    Thu Apr  4 22:55:51 2019       
    +-----------------------------------------------------------------------------+
    | NVIDIA-SMI 410.72       Driver Version: 410.72       CUDA Version: 10.0     |
    |-------------------------------+----------------------+----------------------+
    | GPU  Name        Persistence-M| Bus-Id        Disp.A | Volatile Uncorr. ECC |
    | Fan  Temp  Perf  Pwr:Usage/Cap|         Memory-Usage | GPU-Util  Compute M. |
    |===============================+======================+======================|
    |   0  Tesla T4            Off  | 00000000:00:04.0 Off |                    0 |
    | N/A   73C    P0    32W /  70W |      0MiB / 15079MiB |      0%      Default |
    +-------------------------------+----------------------+----------------------+
    |   1  Tesla T4            Off  | 00000000:00:05.0 Off |                    0 |
    | N/A   70C    P0    32W /  70W |      0MiB / 15079MiB |      0%      Default |
    +-------------------------------+----------------------+----------------------+
                                                                                   
    +-----------------------------------------------------------------------------+
    | Processes:                                                       GPU Memory |
    |  GPU       PID   Type   Process name                             Usage      |
    |=============================================================================|
    |  No running processes found                                                 |
    +-----------------------------------------------------------------------------+



```python
!export CUDA_DEVICE_ORDER="PCI_BUS_ID"
!export CUDA_VISIBLE_DEVICES="0,1"
```


```python
# import resources
import numpy as np
import torch
torch.cuda.empty_cache()
import helper
import datetime
import matplotlib.pyplot as plt
%matplotlib inline

# random seed (for reproducibility)
seed = 1
# set random seed for numpy
np.random.seed(seed)
# set random seed for pytorch
torch.manual_seed(seed)
```




    <torch._C.Generator at 0x7f98f403ca30>




```python
from torchvision import datasets

class RetinaTrain(datasets.ImageFolder):
    """Custom dataset that includes image file paths. Extends
    torchvision.datasets.ImageFolder
    """

    # override the __getitem__ method. this is the method dataloader calls
    def __getitem__(self, index):
        # this is what ImageFolder normally returns 
        original_tuple = super(RetinaTrain, self).__getitem__(index)
        # the image file path
        path = self.imgs[index][0]
        _, path = path.split("train")
        # make a new tuple that includes original and the path
        tuple_with_path = (original_tuple + (path,))
        return tuple_with_path
```


```python
class RetinaTest(datasets.ImageFolder):
    """Custom dataset that includes image file paths. Extends
    torchvision.datasets.ImageFolder
    """

    # override the __getitem__ method. this is the method dataloader calls
    def __getitem__(self, index):
        # this is what ImageFolder normally returns 
        original_tuple = super(RetinaTest, self).__getitem__(index)
        # the image file path
        path = self.imgs[index][0]
        _, path = path.split("test")
        # make a new tuple that includes original and the path
        tuple_with_path = (original_tuple + (path,))
        return tuple_with_path
```


```python
import torchvision.transforms as transforms

# number of subprocesses to use for data loading
num_workers = 6
# how many samples per batch to load
batch_size = 12
print(batch_size)
# convert data to Tensors
transform = transforms.Compose([transforms.Grayscale(num_output_channels=1),
                                transforms.ToTensor()])

# choose the training and test datasets

train_dir = "../dr-dataset/train/"
train_data = RetinaTrain(train_dir, transform=transform)

test_dir = "../dr-dataset/test/"
test_data = RetinaTest(test_dir, transform=transform)

# prepare data loaders
train_loader = torch.utils.data.DataLoader(train_data, 
                                           batch_size=batch_size, 
                                           num_workers=num_workers, shuffle=True)

test_loader = torch.utils.data.DataLoader(test_data, 
                                          batch_size=batch_size, 
                                          num_workers=num_workers, shuffle=False)
```

    12



```python
images, labels, paths = next(iter(train_loader))
helper.imshow(images[0], normalize=True)
print(images[0].shape)
print(labels[0])
print(paths[0])
```

    torch.Size([1, 290, 290])
    tensor(0)
    /0/35892_left.tiff



![png](output_6_1.png)


## First Layer: Convolutional Layer


```python
import torch.nn as nn
import torch.nn.functional as F
```


```python
class ConvLayer(nn.Module):
    
    def __init__(self, in_channels=1, out_channels=256):
        '''Constructs the ConvLayer with a specified input and output size.
           param in_channels: input depth of an image, default value = 1
           param out_channels: output depth of the convolutional layer, default value = 256
           '''
        super(ConvLayer, self).__init__()

        # defining a convolutional layer of the specified size
        self.conv = nn.Conv2d(in_channels, out_channels, 
                              kernel_size=9, stride=1, padding=0)

    def forward(self, x):
        '''Defines the feedforward behavior.
           param x: the input to the layer; an input image
           return: a relu-activated, convolutional layer
           '''
        # applying a ReLu activation to the outputs of the conv layer
        features = F.relu(self.conv(x)) # will have dimensions (batch_size, 256, 282, 282)
#         print("features.shape", features.shape)
        return features
    
```

## Second Layer: Primary Capsules


```python
class PrimaryCaps(nn.Module):
    
    def __init__(self, num_capsules=4, in_channels=256, out_channels=32):
        '''Constructs a list of convolutional layers to be used in 
           creating capsule output vectors.
           param num_capsules: number of capsules to create
           param in_channels: input depth of features, default value = 256
           param out_channels: output depth of the convolutional layers, default value = 32
           '''
        super(PrimaryCaps, self).__init__()

        # creating a list of convolutional layers for each capsule I want to create
        # all capsules have a conv layer with the same parameters
        self.capsules = nn.ModuleList([
            nn.Conv2d(in_channels=in_channels, out_channels=out_channels, 
                      kernel_size=9, stride=2, padding=0)
            for _ in range(num_capsules)])
        print("self.capsules", self.capsules)
    
    def forward(self, x):
        '''Defines the feedforward behavior.
           param x: the input; features from a convolutional layer
           return: a set of normalized, capsule output vectors
           '''
        # get batch size of inputs
        batch_size = x.size(0)
        # reshape convolutional layer outputs to be (batch_size, vector_dim=1152, 1)
        u = [capsule(x).view(batch_size, 32 * 137 * 137, 1) for capsule in self.capsules]
#         print("u", u)
        # stack up output vectors, u, one for each capsule
        u = torch.cat(u, dim=-1)
        # squashing the stack of vectors
        u_squash = self.squash(u)
        return u_squash
    
    def squash(self, input_tensor):
        '''Squashes an input Tensor so it has a magnitude between 0-1.
           param input_tensor: a stack of capsule inputs, s_j
           return: a stack of normalized, capsule output vectors, v_j
           '''
        squared_norm = (input_tensor ** 2).sum(dim=-1, keepdim=True)
        scale = squared_norm / (1 + squared_norm) # normalization coeff
        output_tensor = scale * input_tensor / torch.sqrt(squared_norm)    
        return output_tensor
    
```

## Third Layer: Digit Capsules


```python
import helpers # to get transpose softmax function

# dynamic routing
def dynamic_routing(b_ij, u_hat, squash, routing_iterations=3):
    '''Performs dynamic routing between two capsule layers.
       param b_ij: initial log probabilities that capsule i should be coupled to capsule j
       param u_hat: input, weighted capsule vectors, W u
       param squash: given, normalizing squash function
       param routing_iterations: number of times to update coupling coefficients
       return: v_j, output capsule vectors
       '''    
    # update b_ij, c_ij for number of routing iterations
    for iteration in range(routing_iterations):
        # softmax calculation of coupling coefficients, c_ij
        c_ij = helpers.softmax(b_ij, dim=2)

        # calculating total capsule inputs, s_j = sum(c_ij*u_hat)
        s_j = (c_ij * u_hat).sum(dim=2, keepdim=True)

        # squashing to get a normalized vector output, v_j
        v_j = squash(s_j)

        # if not on the last iteration, calculate agreement and new b_ij
        if iteration < routing_iterations - 1:
            # agreement
            a_ij = (u_hat * v_j).sum(dim=-1, keepdim=True)
            
            # new b_ij
            b_ij = b_ij + a_ij
    
    return v_j # return latest v_j
    
```


```python
# it will also be relevant, in this model, to see if I can train on gpu
TRAIN_ON_GPU = torch.cuda.is_available()

if(TRAIN_ON_GPU):
    print('Training on GPU!')
else:
    print('Only CPU available')
```

    Training on GPU!



```python
class DigitCaps(nn.Module):
    
    def __init__(self, num_capsules=5, previous_layer_nodes=32*137*137, 
                 in_channels=4, out_channels=8):
        '''Constructs an initial weight matrix, W, and sets class variables.
           param num_capsules: number of capsules to create
           param previous_layer_nodes: dimension of input capsule vector, default value = 32*137*137
           param in_channels: number of capsules in previous layer, default value = 4
           param out_channels: dimensions of output capsule vector, default value = 8
           '''
        super(DigitCaps, self).__init__()

        # setting class variables
        self.num_capsules = num_capsules
        self.previous_layer_nodes = previous_layer_nodes # vector input (dim=1152)
        self.in_channels = in_channels # previous layer's number of capsules

        # starting out with a randomly initialized weight matrix, W
        # these will be the weights connecting the PrimaryCaps and DigitCaps layers
        self.W = nn.Parameter(torch.randn(num_capsules, previous_layer_nodes, 
                                          in_channels, out_channels))

    def forward(self, u):
        '''Defines the feedforward behavior.
           param u: the input; vectors from the previous PrimaryCaps layer
           return: a set of normalized, capsule output vectors
           '''
        
        # adding batch_size dims and stacking all u vectors
        u = u[None, :, :, None, :]
        # 4D weight matrix
        W = self.W[:, None, :, :, :]
        
        # calculating u_hat = W*u
        u_hat = torch.matmul(u, W)

        # getting the correct size of b_ij
        # setting them all to 0, initially
        b_ij = torch.zeros(*u_hat.size())
        
        # moving b_ij to GPU, if available
        if TRAIN_ON_GPU:
            b_ij = b_ij.cuda()

        # update coupling coefficients and calculate v_j
        v_j = dynamic_routing(b_ij, u_hat, self.squash, routing_iterations=3)

        return v_j # return final vector outputs
    
    
    def squash(self, input_tensor):
        '''Squashes an input Tensor so it has a magnitude between 0-1.
           param input_tensor: a stack of capsule inputs, s_j
           return: a stack of normalized, capsule output vectors, v_j
           '''
        # same squash function as before
        squared_norm = (input_tensor ** 2).sum(dim=-1, keepdim=True)
        scale = squared_norm / (1 + squared_norm) # normalization coeff
        output_tensor = scale * input_tensor / torch.sqrt(squared_norm)    
        return output_tensor
    
```

# Decoder 

## Linear Layers


```python
class Decoder(nn.Module):
    
    def __init__(self, input_vector_length=8, input_capsules=5, hidden_dim=512):
        '''Constructs an series of linear layers + activations.
           param input_vector_length: dimension of input capsule vector, default value = 16
           param input_capsules: number of capsules in previous layer, default value = 10
           param hidden_dim: dimensions of hidden layers, default value = 512
           '''
        super(Decoder, self).__init__()
        
        # calculate input_dim
        input_dim = input_vector_length * input_capsules
        
        # define linear layers + activations
#         self.linear_layers = nn.Sequential(
#             nn.Linear(input_dim, 295*150), # first hidden layer
#             nn.ReLU(inplace=True),
#             nn.Linear(295*150, 295*295), # second, twice as deep
#             nn.ReLU(inplace=True),
#             nn.Linear(295*295, 290*290), # can be reshaped into 28*28 image
#             nn.Sigmoid() # sigmoid activation to get output pixel values in a range from 0-1
#             )
        
    def forward(self, x):
        '''Defines the feedforward behavior.
           param x: the input; vectors from the previous DigitCaps layer
           return: two things, reconstructed images and the class scores, y
           '''
        classes = (x ** 2).sum(dim=-1) ** 0.5
        classes = F.softmax(classes, dim=-1)

        # find the capsule with the maximum vector length
        # here, vector length indicates the probability of a class' existence
        _, max_length_indices = classes.max(dim=1)
        
        # create a sparse class matrix
        sparse_matrix = torch.eye(5) # 10 is the number of classes
        if TRAIN_ON_GPU:
            sparse_matrix = sparse_matrix.cuda()
        # get the class scores from the "correct" capsule
        y = sparse_matrix.index_select(dim=0, index=max_length_indices.data)
        
        # create reconstructed pixels
#         x = x * y[:, :, None]
        # flatten image into a vector shape (batch_size, vector_dim)
#         flattened_x = x.view(x.size(0), -1)
        # create reconstructed image vectors
#         reconstructions = self.linear_layers(flattened_x)
        
        # return reconstructions and the class scores, y
        return y
```

## Put it All Together 

Finally, I'll use *all* the layers I defined above to create a complete Capsule Network! Recall that the order of these layers is as follows:
1. ConvLayer
2. PrimaryCaps
3. DigitCaps
4. Decoder


```python
class CapsuleNetwork(nn.Module):
    
    def __init__(self):
        '''Constructs a complete Capsule Network.'''
        super(CapsuleNetwork, self).__init__()
        self.conv_layer = ConvLayer()
        self.primary_capsules = PrimaryCaps()
        self.digit_capsules = DigitCaps()
        self.decoder = Decoder()
                
    def forward(self, images):
        '''Defines the feedforward behavior.
           param images: the original MNIST image input data
           return: output of DigitCaps layer, reconstructed images, class scores
           '''
        primary_caps_output = self.primary_capsules(self.conv_layer(images))
        caps_output = self.digit_capsules(primary_caps_output).squeeze().transpose(0,1)
        y = self.decoder(caps_output)
        return caps_output, y
    
```


```python
# instantiate and print net
# device = torch.device("cuda:0")
# print(device)
print("Let's use %d GPUs" % torch.cuda.device_count())
capsule_net = CapsuleNetwork()
capsule_net = nn.DataParallel(capsule_net)
print(capsule_net)
# move model to GPU, if available 
if TRAIN_ON_GPU:
    capsule_net.cuda()
```

    Let's use 2 GPUs
    self.capsules ModuleList(
      (0): Conv2d(256, 32, kernel_size=(9, 9), stride=(2, 2))
      (1): Conv2d(256, 32, kernel_size=(9, 9), stride=(2, 2))
      (2): Conv2d(256, 32, kernel_size=(9, 9), stride=(2, 2))
      (3): Conv2d(256, 32, kernel_size=(9, 9), stride=(2, 2))
    )
    DataParallel(
      (module): CapsuleNetwork(
        (conv_layer): ConvLayer(
          (conv): Conv2d(1, 256, kernel_size=(9, 9), stride=(1, 1))
        )
        (primary_capsules): PrimaryCaps(
          (capsules): ModuleList(
            (0): Conv2d(256, 32, kernel_size=(9, 9), stride=(2, 2))
            (1): Conv2d(256, 32, kernel_size=(9, 9), stride=(2, 2))
            (2): Conv2d(256, 32, kernel_size=(9, 9), stride=(2, 2))
            (3): Conv2d(256, 32, kernel_size=(9, 9), stride=(2, 2))
          )
        )
        (digit_capsules): DigitCaps()
        (decoder): Decoder()
      )
    )


## Custom Loss


```python
class CapsuleLoss(nn.Module):
    
    def __init__(self):
        '''Constructs a CapsuleLoss module.'''
        super(CapsuleLoss, self).__init__()
#         self.reconstruction_loss = nn.MSELoss(size_average=False)

    def forward(self, x, labels, images):
        '''Defines how the loss compares inputs.
           param x: digit capsule outputs
           param labels: 
           param images: the original MNIST image input data
           param reconstructions: reconstructed MNIST image data
           return: weighted margin and reconstruction loss, averaged over a batch
           '''
        batch_size = x.size(0)
        
        ##  calculate the margin loss   ##
        
        # get magnitude of digit capsule vectors, v_c
        v_c = torch.sqrt((x**2).sum(dim=2, keepdim=True))

        # calculate "correct" and incorrect loss
        left = F.relu(0.9 - v_c).view(batch_size, -1)
        right = F.relu(v_c - 0.1).view(batch_size, -1)
        
        # sum the losses, with a lambda = 0.5

        margin_loss = labels * left + 0.5 * (1. - labels) * right
        margin_loss = margin_loss.sum()

        ##  calculate the reconstruction loss   ##
#         images = images.view(reconstructions.size()[0], -1)
#         reconstruction_loss = self.reconstruction_loss(reconstructions, images)

        # return a weighted, summed loss, averaged over a batch size
#         return (margin_loss + 0.0005 * reconstruction_loss) / images.size(0)
        return (margin_loss) / images.size(0)




```

### Specify Loss Function and [Optimizer](http://pytorch.org/docs/stable/optim.html)


```python
import torch.optim as optim

# custom loss
criterion = CapsuleLoss()

# Adam optimizer with default params
optimizer = optim.Adam(capsule_net.parameters())
```

# Train the Network

The steps for training/learning from a batch of data are described in the comments below:
1. Clear the gradients of all optimized variables
2. Forward pass: compute predicted outputs by passing inputs to the model
3. Calculate the loss
4. Backward pass: compute gradient of the loss with respect to model parameters
5. Perform a single optimization step (parameter update)
6. Update average training loss


```python
def train(capsule_net, criterion, optimizer, 
          n_epochs, print_every):
    '''Trains a capsule network and prints out training batch loss statistics.
       Saves model parameters if *validation* loss has decreased.
       param capsule_net: trained capsule network
       param criterion: capsule loss function
       param optimizer: optimizer for updating network weights
       param n_epochs: number of epochs to train for
       param print_every: batches to print and save training loss, default = 100
       return: list of recorded training losses
       '''

    # track training loss over time
    losses, test_losses = [], []
    # one epoch = one pass over all training data 
    for epoch in range(1, n_epochs+1):
        print("inside loop")
        # initialize training loss
        epoch_start = datetime.datetime.now().replace(microsecond=0)
        train_loss = 0.0
        
        capsule_net.train() # set to train mode
    
        # get batches of training image data and targets
        for batch_i, (images, target, path) in enumerate(train_loader):
            # reshape and get target class
#             print(path)
            target = torch.eye(5).index_select(dim=0, index=target)

            if TRAIN_ON_GPU:
                images, target = images.cuda(), target.cuda()

            # zero out gradients
            optimizer.zero_grad()
            # get model outputs
            caps_output, y = capsule_net(images)
            # calculate loss
            loss = criterion(caps_output, target, images)
            # perform backpropagation and optimization
            loss.backward()
            optimizer.step()

            train_loss += loss.item() # accumulated training loss
            
            # print and record training stats
            if batch_i != 0 and batch_i % print_every == 0:
                avg_train_loss = train_loss/print_every
                losses.append(avg_train_loss)
                print('Epoch: {} \tTraining Loss: {:.8f}'.format(epoch, avg_train_loss))
                train_loss = 0 # reset accumulated training loss
        
        else:
            test_loss = 0.0
            accuracy = 0
            
            with torch.no_grad():
                for images, target, paths in test_loader:
                    
                    target = torch.eye(5).index_select(dim=0, index=target)
                    
                    if TRAIN_ON_GPU:
                        images, target = images.cuda(), target.cuda()
                    
                    caps_output, y = capsule_net(images)
                    
                    loss = criterion(caps_output, target, images)
                    
                    test_loss += loss.item()
            test_losses.append(test_loss/len(test_loader))
            print('Epoch: {} \tValidation Loss: {:.8f}'.format(epoch, test_loss/len(test_loader)))
        
        epoch_end = datetime.datetime.now().replace(microsecond=0)
        print(epoch_end-epoch_start)
        save_as = 'saved_model/pytorch_new_' + str(epoch) +'.pt'
        torch.save({
            'epoch': epoch,
            'model_state_dict': capsule_net.state_dict(),
            'optimizer_state_dict': optimizer.state_dict(),
            'loss': losses,
            }, save_as)
        print("Current time: {}".format(datetime.datetime.now().replace(microsecond=0)))
        
    return losses
    
```


```python
# training for 5 epochs
n_epochs = 3
losses = train(capsule_net, criterion, optimizer, n_epochs=n_epochs, print_every=10)
```

    inside loop
    Epoch: 1 	Training Loss: 0.98986534
    Epoch: 1 	Training Loss: 0.89964217
    Epoch: 1 	Training Loss: 0.89935908
    Epoch: 1 	Training Loss: 0.89875549
    Epoch: 1 	Training Loss: 0.89744288
    Epoch: 1 	Training Loss: 0.89435758
    Epoch: 1 	Training Loss: 0.88755296
    Epoch: 1 	Training Loss: 0.87002427
    Epoch: 1 	Training Loss: 0.84099707
    Epoch: 1 	Training Loss: 0.77349980
    Epoch: 1 	Training Loss: 0.70542089
    Epoch: 1 	Training Loss: 0.57993111
    Epoch: 1 	Training Loss: 0.51786025
    Epoch: 1 	Training Loss: 0.45941260
    Epoch: 1 	Training Loss: 0.46109970
    Epoch: 1 	Training Loss: 0.38640765
    Epoch: 1 	Training Loss: 0.37111657
    Epoch: 1 	Training Loss: 0.41167062
    Epoch: 1 	Training Loss: 0.34157209
    Epoch: 1 	Training Loss: 0.33037651
    Epoch: 1 	Training Loss: 0.34009349
    Epoch: 1 	Training Loss: 0.40172360
    Epoch: 1 	Training Loss: 0.32423401
    Epoch: 1 	Training Loss: 0.33807470
    Epoch: 1 	Training Loss: 0.23841776
    Epoch: 1 	Training Loss: 0.30417904
    Epoch: 1 	Training Loss: 0.27781479
    Epoch: 1 	Training Loss: 0.25037113
    Epoch: 1 	Training Loss: 0.20850399
    Epoch: 1 	Training Loss: 0.19985579
    Epoch: 1 	Training Loss: 0.31140163
    Epoch: 1 	Training Loss: 0.23595236
    Epoch: 1 	Training Loss: 0.28039255
    Epoch: 1 	Training Loss: 0.37190324
    Epoch: 1 	Training Loss: 0.27966916
    Epoch: 1 	Training Loss: 0.31423328
    Epoch: 1 	Training Loss: 0.26754214
    Epoch: 1 	Training Loss: 0.29947042
    Epoch: 1 	Training Loss: 0.26236322
    Epoch: 1 	Training Loss: 0.31904239
    Epoch: 1 	Training Loss: 0.21872016
    Epoch: 1 	Training Loss: 0.26983680
    Epoch: 1 	Training Loss: 0.27500070
    Epoch: 1 	Training Loss: 0.21566832
    Epoch: 1 	Training Loss: 0.22866336
    Epoch: 1 	Training Loss: 0.37016698
    Epoch: 1 	Training Loss: 0.42503476
    Epoch: 1 	Training Loss: 0.29699501
    Epoch: 1 	Training Loss: 0.36437488
    Epoch: 1 	Training Loss: 0.33515128
    Epoch: 1 	Training Loss: 0.28468369
    Epoch: 1 	Training Loss: 0.32747684
    Epoch: 1 	Training Loss: 0.28489227
    Epoch: 1 	Training Loss: 0.27481003
    Epoch: 1 	Training Loss: 0.31307813
    Epoch: 1 	Training Loss: 0.27465823
    Epoch: 1 	Training Loss: 0.29296321
    Epoch: 1 	Training Loss: 0.25206350
    Epoch: 1 	Training Loss: 0.30374795
    Epoch: 1 	Training Loss: 0.28393247
    Epoch: 1 	Training Loss: 0.30444986
    Epoch: 1 	Training Loss: 0.30358545
    Epoch: 1 	Training Loss: 0.34684544
    Epoch: 1 	Training Loss: 0.27121533
    Epoch: 1 	Training Loss: 0.30376941
    Epoch: 1 	Training Loss: 0.22038046
    Epoch: 1 	Training Loss: 0.32394411
    Epoch: 1 	Training Loss: 0.25303716
    Epoch: 1 	Training Loss: 0.21512519
    Epoch: 1 	Training Loss: 0.25598515
    Epoch: 1 	Training Loss: 0.21268171
    Epoch: 1 	Training Loss: 0.25430630
    Epoch: 1 	Training Loss: 0.29761939
    Epoch: 1 	Training Loss: 0.28211797
    Epoch: 1 	Training Loss: 0.33551254
    Epoch: 1 	Training Loss: 0.25317675
    Epoch: 1 	Training Loss: 0.29611355
    Epoch: 1 	Training Loss: 0.27520526
    Epoch: 1 	Training Loss: 0.27281468
    Epoch: 1 	Training Loss: 0.26347474
    Epoch: 1 	Training Loss: 0.40374655
    Epoch: 1 	Training Loss: 0.40533767
    Epoch: 1 	Training Loss: 0.32739204
    Epoch: 1 	Training Loss: 0.27549793
    Epoch: 1 	Training Loss: 0.35385906
    Epoch: 1 	Training Loss: 0.28184080
    Epoch: 1 	Training Loss: 0.26221307
    Epoch: 1 	Training Loss: 0.33233148
    Epoch: 1 	Training Loss: 0.31156510
    Epoch: 1 	Training Loss: 0.27164449
    Epoch: 1 	Training Loss: 0.30127179
    Epoch: 1 	Training Loss: 0.26387546
    Epoch: 1 	Training Loss: 0.26507204
    Epoch: 1 	Training Loss: 0.27211448
    Epoch: 1 	Training Loss: 0.29052486
    Epoch: 1 	Training Loss: 0.25132173
    Epoch: 1 	Training Loss: 0.25052546
    Epoch: 1 	Training Loss: 0.24141876
    Epoch: 1 	Training Loss: 0.25151985
    Epoch: 1 	Training Loss: 0.28216954
    Epoch: 1 	Training Loss: 0.34140209
    Epoch: 1 	Training Loss: 0.28148255
    Epoch: 1 	Training Loss: 0.26290096
    Epoch: 1 	Training Loss: 0.34263929
    Epoch: 1 	Training Loss: 0.20163987
    Epoch: 1 	Training Loss: 0.33230772
    Epoch: 1 	Training Loss: 0.27355765
    Epoch: 1 	Training Loss: 0.34241157
    Epoch: 1 	Training Loss: 0.27218771
    Epoch: 1 	Training Loss: 0.25209130
    Epoch: 1 	Training Loss: 0.30281054
    Epoch: 1 	Training Loss: 0.26110047
    Epoch: 1 	Training Loss: 0.17020417
    Epoch: 1 	Training Loss: 0.26033590
    Epoch: 1 	Training Loss: 0.32190457
    Epoch: 1 	Training Loss: 0.33278610
    Epoch: 1 	Training Loss: 0.18162348
    Epoch: 1 	Training Loss: 0.29127496
    Epoch: 1 	Training Loss: 0.38291903
    Epoch: 1 	Training Loss: 0.26179002
    Epoch: 1 	Training Loss: 0.28115844
    Epoch: 1 	Training Loss: 0.29175485
    Epoch: 1 	Training Loss: 0.31114067
    Epoch: 1 	Training Loss: 0.31163870
    Epoch: 1 	Training Loss: 0.32108347
    Epoch: 1 	Training Loss: 0.23034281
    Epoch: 1 	Training Loss: 0.27073459
    Epoch: 1 	Training Loss: 0.27049191
    Epoch: 1 	Training Loss: 0.31069113
    Epoch: 1 	Training Loss: 0.32082036
    Epoch: 1 	Training Loss: 0.22063556
    Epoch: 1 	Training Loss: 0.33075423
    Epoch: 1 	Training Loss: 0.23064427
    Epoch: 1 	Training Loss: 0.28083089
    Epoch: 1 	Training Loss: 0.29023265
    Epoch: 1 	Training Loss: 0.27048821
    Epoch: 1 	Training Loss: 0.26050138
    Epoch: 1 	Training Loss: 0.27041694
    Epoch: 1 	Training Loss: 0.31082055
    Epoch: 1 	Training Loss: 0.27067643
    Epoch: 1 	Training Loss: 0.31051269
    Epoch: 1 	Training Loss: 0.21049009
    Epoch: 1 	Training Loss: 0.27067521
    Epoch: 1 	Training Loss: 0.30059913
    Epoch: 1 	Training Loss: 0.37128851
    Epoch: 1 	Training Loss: 0.25100243
    Epoch: 1 	Training Loss: 0.32096276
    Epoch: 1 	Training Loss: 0.27058229
    Epoch: 1 	Training Loss: 0.29044801
    Epoch: 1 	Training Loss: 0.18039479
    Epoch: 1 	Training Loss: 0.31114567
    Epoch: 1 	Training Loss: 0.23121694
    Epoch: 1 	Training Loss: 0.32053291
    Epoch: 1 	Training Loss: 0.18053465
    Epoch: 1 	Training Loss: 0.31131582
    Epoch: 1 	Training Loss: 0.20070193
    Epoch: 1 	Training Loss: 0.35130484
    Epoch: 1 	Training Loss: 0.32151923
    Epoch: 1 	Training Loss: 0.29194285
    Epoch: 1 	Training Loss: 0.24049539
    Epoch: 1 	Training Loss: 0.24044782
    Epoch: 1 	Training Loss: 0.28111607
    Epoch: 1 	Training Loss: 0.26054095
    Epoch: 1 	Training Loss: 0.21039355
    Epoch: 1 	Training Loss: 0.27070762
    Epoch: 1 	Training Loss: 0.28052572
    Epoch: 1 	Training Loss: 0.16041543
    Epoch: 1 	Training Loss: 0.39100830
    Epoch: 1 	Training Loss: 0.23032017
    Epoch: 1 	Training Loss: 0.27060654
    Epoch: 1 	Training Loss: 0.25053713
    Epoch: 1 	Training Loss: 0.27049412
    Epoch: 1 	Training Loss: 0.23037810
    Epoch: 1 	Training Loss: 0.37109964
    Epoch: 1 	Training Loss: 0.36142235
    Epoch: 1 	Training Loss: 0.22046773
    Epoch: 1 	Training Loss: 0.30040130
    Epoch: 1 	Training Loss: 0.27038779
    Epoch: 1 	Training Loss: 0.23039300
    Epoch: 1 	Training Loss: 0.26040001
    Epoch: 1 	Training Loss: 0.35053587
    Epoch: 1 	Training Loss: 0.24056833
    Epoch: 1 	Training Loss: 0.24056695
    Epoch: 1 	Training Loss: 0.37027180
    Epoch: 1 	Training Loss: 0.32048873
    Epoch: 1 	Training Loss: 0.22041851
    Epoch: 1 	Training Loss: 0.29064892
    Epoch: 1 	Training Loss: 0.28029549
    Epoch: 1 	Training Loss: 0.38052852
    Epoch: 1 	Training Loss: 0.30078690
    Epoch: 1 	Training Loss: 0.24027963
    Epoch: 1 	Training Loss: 0.33037131
    Epoch: 1 	Training Loss: 0.25019766
    Epoch: 1 	Training Loss: 0.30077961
    Epoch: 1 	Training Loss: 0.30049339
    Epoch: 1 	Training Loss: 0.34025180
    Epoch: 1 	Training Loss: 0.24041610
    Epoch: 1 	Training Loss: 0.29035702
    Epoch: 1 	Training Loss: 0.23025248
    Epoch: 1 	Training Loss: 0.26026973
    Epoch: 1 	Training Loss: 0.29051811
    Epoch: 1 	Training Loss: 0.34063064
    Epoch: 1 	Training Loss: 0.30038224
    Epoch: 1 	Training Loss: 0.27032804
    Epoch: 1 	Training Loss: 0.26045082
    Epoch: 1 	Training Loss: 0.32077632
    Epoch: 1 	Training Loss: 0.24023791
    Epoch: 1 	Training Loss: 0.23025333
    Epoch: 1 	Training Loss: 0.30045204
    Epoch: 1 	Training Loss: 0.24026737
    Epoch: 1 	Training Loss: 0.28049809
    Epoch: 1 	Training Loss: 0.34061946
    Epoch: 1 	Training Loss: 0.24065402
    Epoch: 1 	Training Loss: 0.20064241
    Epoch: 1 	Training Loss: 0.33100652
    Epoch: 1 	Training Loss: 0.38119024
    Epoch: 1 	Training Loss: 0.22034208
    Epoch: 1 	Training Loss: 0.26063132
    Epoch: 1 	Training Loss: 0.35108764
    Epoch: 1 	Training Loss: 0.28071230
    Epoch: 1 	Training Loss: 0.32075540
    Epoch: 1 	Training Loss: 0.21049958
    Epoch: 1 	Training Loss: 0.34046679
    Epoch: 1 	Training Loss: 0.28044805
    Epoch: 1 	Training Loss: 0.33052209
    Epoch: 1 	Training Loss: 0.32100782
    Epoch: 1 	Training Loss: 0.30073975
    Epoch: 1 	Training Loss: 0.26035504
    Epoch: 1 	Training Loss: 0.20035792
    Epoch: 1 	Training Loss: 0.29033589
    Epoch: 1 	Training Loss: 0.26039623
    Epoch: 1 	Training Loss: 0.25029390
    Epoch: 1 	Training Loss: 0.26080301
    Epoch: 1 	Training Loss: 0.21073143
    Epoch: 1 	Training Loss: 0.34103574
    Epoch: 1 	Training Loss: 0.24048904
    Epoch: 1 	Training Loss: 0.40097059
    Epoch: 1 	Training Loss: 0.33057960
    Epoch: 1 	Training Loss: 0.39071091
    Epoch: 1 	Training Loss: 0.35041786
    Epoch: 1 	Training Loss: 0.26051782
    Epoch: 1 	Training Loss: 0.34077442
    Epoch: 1 	Training Loss: 0.28028952
    Epoch: 1 	Training Loss: 0.32057509
    Epoch: 1 	Training Loss: 0.20034306
    Epoch: 1 	Training Loss: 0.27057423
    Epoch: 1 	Training Loss: 0.30046229
    Epoch: 1 	Training Loss: 0.22065800
    Epoch: 1 	Training Loss: 0.34070745
    Epoch: 1 	Training Loss: 0.28036735
    Epoch: 1 	Training Loss: 0.27083153
    Epoch: 1 	Training Loss: 0.24066681
    Epoch: 1 	Training Loss: 0.21033666
    Epoch: 1 	Training Loss: 0.28064208
    Epoch: 1 	Training Loss: 0.37032715
    Epoch: 1 	Training Loss: 0.28066964
    Epoch: 1 	Training Loss: 0.35047008
    Epoch: 1 	Training Loss: 0.20039919
    Epoch: 1 	Training Loss: 0.24079405
    Epoch: 1 	Training Loss: 0.22016803
    Epoch: 1 	Training Loss: 0.22045429
    Epoch: 1 	Training Loss: 0.24047604
    Epoch: 1 	Training Loss: 0.29047960
    Epoch: 1 	Training Loss: 0.32029584
    Epoch: 1 	Training Loss: 0.24046301
    Epoch: 1 	Training Loss: 0.25027988
    Epoch: 1 	Training Loss: 0.26050172
    Epoch: 1 	Training Loss: 0.32080556
    Epoch: 1 	Training Loss: 0.28048014
    Epoch: 1 	Validation Loss: 0.76283560
    1:42:35
    Current time: 2019-04-05 00:39:05
    inside loop
    Epoch: 2 	Training Loss: 0.38091820
    Epoch: 2 	Training Loss: 0.35068109
    Epoch: 2 	Training Loss: 0.27064111
    Epoch: 2 	Training Loss: 0.20046761
    Epoch: 2 	Training Loss: 0.27039514
    Epoch: 2 	Training Loss: 0.32066541
    Epoch: 2 	Training Loss: 0.31043063
    Epoch: 2 	Training Loss: 0.25038487
    Epoch: 2 	Training Loss: 0.29022153
    Epoch: 2 	Training Loss: 0.25019807
    Epoch: 2 	Training Loss: 0.32067677
    Epoch: 2 	Training Loss: 0.22026568
    Epoch: 2 	Training Loss: 0.31052899
    Epoch: 2 	Training Loss: 0.24053976
    Epoch: 2 	Training Loss: 0.31059875
    Epoch: 2 	Training Loss: 0.31057481
    Epoch: 2 	Training Loss: 0.25082203
    Epoch: 2 	Training Loss: 0.30050241
    Epoch: 2 	Training Loss: 0.28045387
    Epoch: 2 	Training Loss: 0.31019279
    Epoch: 2 	Training Loss: 0.25029296
    Epoch: 2 	Training Loss: 0.28037737
    Epoch: 2 	Training Loss: 0.23047559
    Epoch: 2 	Training Loss: 0.28083658
    Epoch: 2 	Training Loss: 0.32028942
    Epoch: 2 	Training Loss: 0.26069567
    Epoch: 2 	Training Loss: 0.24025137
    Epoch: 2 	Training Loss: 0.26042423
    Epoch: 2 	Training Loss: 0.31058552
    Epoch: 2 	Training Loss: 0.38095237
    Epoch: 2 	Training Loss: 0.26092470
    Epoch: 2 	Training Loss: 0.32139521
    Epoch: 2 	Training Loss: 0.31062223
    Epoch: 2 	Training Loss: 0.20057711
    Epoch: 2 	Training Loss: 0.21081867
    Epoch: 2 	Training Loss: 0.34077999
    Epoch: 2 	Training Loss: 0.22078799
    Epoch: 2 	Training Loss: 0.20132460
    Epoch: 2 	Training Loss: 0.24059089
    Epoch: 2 	Training Loss: 0.25031351
    Epoch: 2 	Training Loss: 0.34065573
    Epoch: 2 	Training Loss: 0.26038287
    Epoch: 2 	Training Loss: 0.29032853
    Epoch: 2 	Training Loss: 0.24028094
    Epoch: 2 	Training Loss: 0.21030414
    Epoch: 2 	Training Loss: 0.25041389
    Epoch: 2 	Training Loss: 0.33039687
    Epoch: 2 	Training Loss: 0.34047524
    Epoch: 2 	Training Loss: 0.29065786
    Epoch: 2 	Training Loss: 0.21057760
    Epoch: 2 	Training Loss: 0.30093207
    Epoch: 2 	Training Loss: 0.27048685
    Epoch: 2 	Training Loss: 0.26051823
    Epoch: 2 	Training Loss: 0.22025289
    Epoch: 2 	Training Loss: 0.27024461
    Epoch: 2 	Training Loss: 0.42056443
    Epoch: 2 	Training Loss: 0.20055326
    Epoch: 2 	Training Loss: 0.27037606
    Epoch: 2 	Training Loss: 0.29027835
    Epoch: 2 	Training Loss: 0.32062352
    Epoch: 2 	Training Loss: 0.20068428
    Epoch: 2 	Training Loss: 0.35040848
    Epoch: 2 	Training Loss: 0.30017489
    Epoch: 2 	Training Loss: 0.29025777
    Epoch: 2 	Training Loss: 0.27034994
    Epoch: 2 	Training Loss: 0.28059495
    Epoch: 2 	Training Loss: 0.29053482
    Epoch: 2 	Training Loss: 0.31094933
    Epoch: 2 	Training Loss: 0.29087066
    Epoch: 2 	Training Loss: 0.29033761
    Epoch: 2 	Training Loss: 0.31082179
    Epoch: 2 	Training Loss: 0.18057706
    Epoch: 2 	Training Loss: 0.28080454
    Epoch: 2 	Training Loss: 0.26078761
    Epoch: 2 	Training Loss: 0.36058331
    Epoch: 2 	Training Loss: 0.31036073
    Epoch: 2 	Training Loss: 0.25078187
    Epoch: 2 	Training Loss: 0.36096905
    Epoch: 2 	Training Loss: 0.23046928
    Epoch: 2 	Training Loss: 0.21039105
    Epoch: 2 	Training Loss: 0.26043244
    Epoch: 2 	Training Loss: 0.27040791
    Epoch: 2 	Training Loss: 0.28021434
    Epoch: 2 	Training Loss: 0.16017107
    Epoch: 2 	Training Loss: 0.35034540
    Epoch: 2 	Training Loss: 0.25050832
    Epoch: 2 	Training Loss: 0.28075991
    Epoch: 2 	Training Loss: 0.22079558
    Epoch: 2 	Training Loss: 0.28058263
    Epoch: 2 	Training Loss: 0.26048312
    Epoch: 2 	Training Loss: 0.33060118
    Epoch: 2 	Training Loss: 0.18051912
    Epoch: 2 	Training Loss: 0.36057160
    Epoch: 2 	Training Loss: 0.17033018
    Epoch: 2 	Training Loss: 0.14021388
    Epoch: 2 	Training Loss: 0.30060969
    Epoch: 2 	Training Loss: 0.24082047
    Epoch: 2 	Training Loss: 0.22052837
    Epoch: 2 	Training Loss: 0.23056728
    Epoch: 2 	Training Loss: 0.28045210
    Epoch: 2 	Training Loss: 0.34044865
    Epoch: 2 	Training Loss: 0.32043338
    Epoch: 2 	Training Loss: 0.29038950
    Epoch: 2 	Training Loss: 0.26041496
    Epoch: 2 	Training Loss: 0.33044866
    Epoch: 2 	Training Loss: 0.32054923
    Epoch: 2 	Training Loss: 0.26073758
    Epoch: 2 	Training Loss: 0.27080546
    Epoch: 2 	Training Loss: 0.29058326
    Epoch: 2 	Training Loss: 0.32070995
    Epoch: 2 	Training Loss: 0.32070641
    Epoch: 2 	Training Loss: 0.24057285
    Epoch: 2 	Training Loss: 0.27075294
    Epoch: 2 	Training Loss: 0.32119584
    Epoch: 2 	Training Loss: 0.29067072
    Epoch: 2 	Training Loss: 0.17087310
    Epoch: 2 	Training Loss: 0.35082467
    Epoch: 2 	Training Loss: 0.27029552
    Epoch: 2 	Training Loss: 0.27051577
    Epoch: 2 	Training Loss: 0.35043548
    Epoch: 2 	Training Loss: 0.31032463
    Epoch: 2 	Training Loss: 0.30070341
    Epoch: 2 	Training Loss: 0.24030002
    Epoch: 2 	Training Loss: 0.28058945
    Epoch: 2 	Training Loss: 0.25094825
    Epoch: 2 	Training Loss: 0.32057433
    Epoch: 2 	Training Loss: 0.34038327
    Epoch: 2 	Training Loss: 0.30033604
    Epoch: 2 	Training Loss: 0.31086700
    Epoch: 2 	Training Loss: 0.31034506
    Epoch: 2 	Training Loss: 0.29037183
    Epoch: 2 	Training Loss: 0.26045365
    Epoch: 2 	Training Loss: 0.27078478
    Epoch: 2 	Training Loss: 0.28069629
    Epoch: 2 	Training Loss: 0.32069317
    Epoch: 2 	Training Loss: 0.34089030
    Epoch: 2 	Training Loss: 0.26064956
    Epoch: 2 	Training Loss: 0.24055002
    Epoch: 2 	Training Loss: 0.28039212
    Epoch: 2 	Training Loss: 0.26020980
    Epoch: 2 	Training Loss: 0.29023223
    Epoch: 2 	Training Loss: 0.26028326
    Epoch: 2 	Training Loss: 0.31034374
    Epoch: 2 	Training Loss: 0.28015612
    Epoch: 2 	Training Loss: 0.24030636
    Epoch: 2 	Training Loss: 0.39048485
    Epoch: 2 	Training Loss: 0.25054206
    Epoch: 2 	Training Loss: 0.30065411
    Epoch: 2 	Training Loss: 0.25021431
    Epoch: 2 	Training Loss: 0.22031298
    Epoch: 2 	Training Loss: 0.31040480
    Epoch: 2 	Training Loss: 0.28053303
    Epoch: 2 	Training Loss: 0.29072984
    Epoch: 2 	Training Loss: 0.25037313
    Epoch: 2 	Training Loss: 0.35045030
    Epoch: 2 	Training Loss: 0.26053122
    Epoch: 2 	Training Loss: 0.31070401
    Epoch: 2 	Training Loss: 0.21032477
    Epoch: 2 	Training Loss: 0.20025293
    Epoch: 2 	Training Loss: 0.32063662
    Epoch: 2 	Training Loss: 0.26028212
    Epoch: 2 	Training Loss: 0.21053300
    Epoch: 2 	Training Loss: 0.30091646
    Epoch: 2 	Training Loss: 0.30049650
    Epoch: 2 	Training Loss: 0.30037564
    Epoch: 2 	Training Loss: 0.25056689
    Epoch: 2 	Training Loss: 0.29062397
    Epoch: 2 	Training Loss: 0.24029674
    Epoch: 2 	Training Loss: 0.27027305
    Epoch: 2 	Training Loss: 0.25047824
    Epoch: 2 	Training Loss: 0.34039708
    Epoch: 2 	Training Loss: 0.33056842
    Epoch: 2 	Training Loss: 0.32072798
    Epoch: 2 	Training Loss: 0.31049913
    Epoch: 2 	Training Loss: 0.30041612
    Epoch: 2 	Training Loss: 0.26055329
    Epoch: 2 	Training Loss: 0.29058337
    Epoch: 2 	Training Loss: 0.22040239
    Epoch: 2 	Training Loss: 0.22050041
    Epoch: 2 	Training Loss: 0.31053634
    Epoch: 2 	Training Loss: 0.28033555
    Epoch: 2 	Training Loss: 0.29064364
    Epoch: 2 	Training Loss: 0.26020378
    Epoch: 2 	Training Loss: 0.36058778
    Epoch: 2 	Training Loss: 0.39051859
    Epoch: 2 	Training Loss: 0.32109850
    Epoch: 2 	Training Loss: 0.27065234
    Epoch: 2 	Training Loss: 0.40063072
    Epoch: 2 	Training Loss: 0.24042526
    Epoch: 2 	Training Loss: 0.29044476
    Epoch: 2 	Training Loss: 0.25053458
    Epoch: 2 	Training Loss: 0.30038702
    Epoch: 2 	Training Loss: 0.42086758
    Epoch: 2 	Training Loss: 0.20067442
    Epoch: 2 	Training Loss: 0.26044679
    Epoch: 2 	Training Loss: 0.26026590
    Epoch: 2 	Training Loss: 0.28052205
    Epoch: 2 	Training Loss: 0.36111756
    Epoch: 2 	Training Loss: 0.20063288
    Epoch: 2 	Training Loss: 0.27040572
    Epoch: 2 	Training Loss: 0.28035812
    Epoch: 2 	Training Loss: 0.25056531
    Epoch: 2 	Training Loss: 0.21043517
    Epoch: 2 	Training Loss: 0.27050644
    Epoch: 2 	Training Loss: 0.17035518
    Epoch: 2 	Training Loss: 0.28036475
    Epoch: 2 	Training Loss: 0.33026496
    Epoch: 2 	Training Loss: 0.27042815
    Epoch: 2 	Training Loss: 0.25052283
    Epoch: 2 	Training Loss: 0.32063524
    Epoch: 2 	Training Loss: 0.29057554
    Epoch: 2 	Training Loss: 0.21044429
    Epoch: 2 	Training Loss: 0.36040890
    Epoch: 2 	Training Loss: 0.27024204
    Epoch: 2 	Training Loss: 0.34027508
    Epoch: 2 	Training Loss: 0.31080927
    Epoch: 2 	Training Loss: 0.31039728
    Epoch: 2 	Training Loss: 0.24029395
    Epoch: 2 	Training Loss: 0.27032555
    Epoch: 2 	Training Loss: 0.31025053
    Epoch: 2 	Training Loss: 0.27029699
    Epoch: 2 	Training Loss: 0.22041440
    Epoch: 2 	Training Loss: 0.28061001
    Epoch: 2 	Training Loss: 0.33057578
    Epoch: 2 	Training Loss: 0.34091280
    Epoch: 2 	Training Loss: 0.26084157
    Epoch: 2 	Training Loss: 0.22031673
    Epoch: 2 	Training Loss: 0.26030126
    Epoch: 2 	Training Loss: 0.33015713
    Epoch: 2 	Training Loss: 0.19035410
    Epoch: 2 	Training Loss: 0.18032066
    Epoch: 2 	Training Loss: 0.29015917
    Epoch: 2 	Training Loss: 0.24030477
    Epoch: 2 	Training Loss: 0.29045597
    Epoch: 2 	Training Loss: 0.25067969
    Epoch: 2 	Training Loss: 0.28033615
    Epoch: 2 	Training Loss: 0.27036825
    Epoch: 2 	Training Loss: 0.27053594
    Epoch: 2 	Training Loss: 0.37053042
    Epoch: 2 	Training Loss: 0.30045368
    Epoch: 2 	Training Loss: 0.22043983
    Epoch: 2 	Training Loss: 0.34051769
    Epoch: 2 	Training Loss: 0.30054531
    Epoch: 2 	Training Loss: 0.19042905
    Epoch: 2 	Training Loss: 0.35106413
    Epoch: 2 	Training Loss: 0.38083882
    Epoch: 2 	Training Loss: 0.28067417
    Epoch: 2 	Training Loss: 0.30102962
    Epoch: 2 	Training Loss: 0.35039344
    Epoch: 2 	Training Loss: 0.26045075
    Epoch: 2 	Training Loss: 0.36066367
    Epoch: 2 	Training Loss: 0.33054707
    Epoch: 2 	Training Loss: 0.26036069
    Epoch: 2 	Training Loss: 0.35042041
    Epoch: 2 	Training Loss: 0.38036543
    Epoch: 2 	Training Loss: 0.25055673
    Epoch: 2 	Training Loss: 0.18075034
    Epoch: 2 	Training Loss: 0.26061900
    Epoch: 2 	Training Loss: 0.29095082
    Epoch: 2 	Training Loss: 0.26057812
    Epoch: 2 	Training Loss: 0.21044585
    Epoch: 2 	Training Loss: 0.19065688
    Epoch: 2 	Training Loss: 0.31083897
    Epoch: 2 	Training Loss: 0.28060824
    Epoch: 2 	Training Loss: 0.32099272
    Epoch: 2 	Training Loss: 0.21024366
    Epoch: 2 	Training Loss: 0.26036387
    Epoch: 2 	Training Loss: 0.29038528
    Epoch: 2 	Training Loss: 0.26052134
    Epoch: 2 	Validation Loss: 0.76336010
    1:41:56
    Current time: 2019-04-05 02:21:03
    inside loop
    Epoch: 3 	Training Loss: 0.31044063
    Epoch: 3 	Training Loss: 0.38063576
    Epoch: 3 	Training Loss: 0.27073760
    Epoch: 3 	Training Loss: 0.32068283
    Epoch: 3 	Training Loss: 0.27056940
    Epoch: 3 	Training Loss: 0.26037755
    Epoch: 3 	Training Loss: 0.25050065
    Epoch: 3 	Training Loss: 0.24027854
    Epoch: 3 	Training Loss: 0.26043352
    Epoch: 3 	Training Loss: 0.28028791
    Epoch: 3 	Training Loss: 0.25027180
    Epoch: 3 	Training Loss: 0.28024478
    Epoch: 3 	Training Loss: 0.27031827
    Epoch: 3 	Training Loss: 0.30070507
    Epoch: 3 	Training Loss: 0.26046146
    Epoch: 3 	Training Loss: 0.33033794
    Epoch: 3 	Training Loss: 0.28076789
    Epoch: 3 	Training Loss: 0.26051659
    Epoch: 3 	Training Loss: 0.37050471
    Epoch: 3 	Training Loss: 0.31047592
    Epoch: 3 	Training Loss: 0.26034421
    Epoch: 3 	Training Loss: 0.30077768
    Epoch: 3 	Training Loss: 0.25047048
    Epoch: 3 	Training Loss: 0.22070351
    Epoch: 3 	Training Loss: 0.34045032
    Epoch: 3 	Training Loss: 0.26023908
    Epoch: 3 	Training Loss: 0.31053717
    Epoch: 3 	Training Loss: 0.25047608
    Epoch: 3 	Training Loss: 0.30049743
    Epoch: 3 	Training Loss: 0.24052254
    Epoch: 3 	Training Loss: 0.25019488
    Epoch: 3 	Training Loss: 0.32028551
    Epoch: 3 	Training Loss: 0.28026776
    Epoch: 3 	Training Loss: 0.23031525
    Epoch: 3 	Training Loss: 0.22027218
    Epoch: 3 	Training Loss: 0.27018035
    Epoch: 3 	Training Loss: 0.22017675
    Epoch: 3 	Training Loss: 0.25060288
    Epoch: 3 	Training Loss: 0.29034738
    Epoch: 3 	Training Loss: 0.26025504
    Epoch: 3 	Training Loss: 0.30024718
    Epoch: 3 	Training Loss: 0.25016202
    Epoch: 3 	Training Loss: 0.26034680
    Epoch: 3 	Training Loss: 0.26040866
    Epoch: 3 	Training Loss: 0.20024115
    Epoch: 3 	Training Loss: 0.37049541
    Epoch: 3 	Training Loss: 0.24028698
    Epoch: 3 	Training Loss: 0.30050683
    Epoch: 3 	Training Loss: 0.33033375
    Epoch: 3 	Training Loss: 0.25015939
    Epoch: 3 	Training Loss: 0.32029030
    Epoch: 3 	Training Loss: 0.26045896
    Epoch: 3 	Training Loss: 0.40061270
    Epoch: 3 	Training Loss: 0.23060828
    Epoch: 3 	Training Loss: 0.22064911
    Epoch: 3 	Training Loss: 0.32057549
    Epoch: 3 	Training Loss: 0.34030742
    Epoch: 3 	Training Loss: 0.28033247
    Epoch: 3 	Training Loss: 0.29047277
    Epoch: 3 	Training Loss: 0.34043292
    Epoch: 3 	Training Loss: 0.24046776
    Epoch: 3 	Training Loss: 0.29042058
    Epoch: 3 	Training Loss: 0.26038870
    Epoch: 3 	Training Loss: 0.19036051
    Epoch: 3 	Training Loss: 0.27039189
    Epoch: 3 	Training Loss: 0.32055918
    Epoch: 3 	Training Loss: 0.28043121
    Epoch: 3 	Training Loss: 0.24075209
    Epoch: 3 	Training Loss: 0.24045879
    Epoch: 3 	Training Loss: 0.37052097
    Epoch: 3 	Training Loss: 0.33060717
    Epoch: 3 	Training Loss: 0.31089860
    Epoch: 3 	Training Loss: 0.29094662
    Epoch: 3 	Training Loss: 0.22052748
    Epoch: 3 	Training Loss: 0.22023720
    Epoch: 3 	Training Loss: 0.27049808
    Epoch: 3 	Training Loss: 0.25100626
    Epoch: 3 	Training Loss: 0.21044445
    Epoch: 3 	Training Loss: 0.32039155
    Epoch: 3 	Training Loss: 0.21030834
    Epoch: 3 	Training Loss: 0.25020166
    Epoch: 3 	Training Loss: 0.20031737
    Epoch: 3 	Training Loss: 0.37046039
    Epoch: 3 	Training Loss: 0.32034878
    Epoch: 3 	Training Loss: 0.31021594
    Epoch: 3 	Training Loss: 0.33052934
    Epoch: 3 	Training Loss: 0.37068916
    Epoch: 3 	Training Loss: 0.27042407
    Epoch: 3 	Training Loss: 0.31033995
    Epoch: 3 	Training Loss: 0.29032239
    Epoch: 3 	Training Loss: 0.24043286
    Epoch: 3 	Training Loss: 0.32049217
    Epoch: 3 	Training Loss: 0.27045474
    Epoch: 3 	Training Loss: 0.22023832
    Epoch: 3 	Training Loss: 0.33072895
    Epoch: 3 	Training Loss: 0.37097613
    Epoch: 3 	Training Loss: 0.32060405
    Epoch: 3 	Training Loss: 0.27034475
    Epoch: 3 	Training Loss: 0.24039930
    Epoch: 3 	Training Loss: 0.32045612
    Epoch: 3 	Training Loss: 0.25097340
    Epoch: 3 	Training Loss: 0.19063536
    Epoch: 3 	Training Loss: 0.34073020
    Epoch: 3 	Training Loss: 0.32045011
    Epoch: 3 	Training Loss: 0.34038668
    Epoch: 3 	Training Loss: 0.28032170
    Epoch: 3 	Training Loss: 0.26025484
    Epoch: 3 	Training Loss: 0.26032016
    Epoch: 3 	Training Loss: 0.27030602
    Epoch: 3 	Training Loss: 0.26043480
    Epoch: 3 	Training Loss: 0.37101881
    Epoch: 3 	Training Loss: 0.27053495
    Epoch: 3 	Training Loss: 0.29038581
    Epoch: 3 	Training Loss: 0.26039886
    Epoch: 3 	Training Loss: 0.24028313
    Epoch: 3 	Training Loss: 0.26044571
    Epoch: 3 	Training Loss: 0.26023465
    Epoch: 3 	Training Loss: 0.27043328
    Epoch: 3 	Training Loss: 0.26034059
    Epoch: 3 	Training Loss: 0.25029869
    Epoch: 3 	Training Loss: 0.24036409
    Epoch: 3 	Training Loss: 0.29031789
    Epoch: 3 	Training Loss: 0.26079401
    Epoch: 3 	Training Loss: 0.30054685
    Epoch: 3 	Training Loss: 0.24074020
    Epoch: 3 	Training Loss: 0.27060750
    Epoch: 3 	Training Loss: 0.23055151
    Epoch: 3 	Training Loss: 0.29033038
    Epoch: 3 	Training Loss: 0.22016652
    Epoch: 3 	Training Loss: 0.21040450
    Epoch: 3 	Training Loss: 0.34032512
    Epoch: 3 	Training Loss: 0.22032284
    Epoch: 3 	Training Loss: 0.34046733
    Epoch: 3 	Training Loss: 0.26027833
    Epoch: 3 	Training Loss: 0.27062872
    Epoch: 3 	Training Loss: 0.22024351
    Epoch: 3 	Training Loss: 0.22017093
    Epoch: 3 	Training Loss: 0.23027188
    Epoch: 3 	Training Loss: 0.33061737
    Epoch: 3 	Training Loss: 0.34034317
    Epoch: 3 	Training Loss: 0.27033799
    Epoch: 3 	Training Loss: 0.23036947
    Epoch: 3 	Training Loss: 0.33026565
    Epoch: 3 	Training Loss: 0.24020289
    Epoch: 3 	Training Loss: 0.28029469
    Epoch: 3 	Training Loss: 0.26027800
    Epoch: 3 	Training Loss: 0.31028094
    Epoch: 3 	Training Loss: 0.24021067
    Epoch: 3 	Training Loss: 0.33034048
    Epoch: 3 	Training Loss: 0.34033824
    Epoch: 3 	Training Loss: 0.25040695
    Epoch: 3 	Training Loss: 0.33040790
    Epoch: 3 	Training Loss: 0.30034436
    Epoch: 3 	Training Loss: 0.23042586
    Epoch: 3 	Training Loss: 0.28038693
    Epoch: 3 	Training Loss: 0.37072739
    Epoch: 3 	Training Loss: 0.30101998
    Epoch: 3 	Training Loss: 0.33116714
    Epoch: 3 	Training Loss: 0.27084519
    Epoch: 3 	Training Loss: 0.27103973
    Epoch: 3 	Training Loss: 0.26053637
    Epoch: 3 	Training Loss: 0.24066526
    Epoch: 3 	Training Loss: 0.30052273
    Epoch: 3 	Training Loss: 0.27047929
    Epoch: 3 	Training Loss: 0.33050598
    Epoch: 3 	Training Loss: 0.25036011
    Epoch: 3 	Training Loss: 0.28030200
    Epoch: 3 	Training Loss: 0.28015002
    Epoch: 3 	Training Loss: 0.29029428
    Epoch: 3 	Training Loss: 0.25054534
    Epoch: 3 	Training Loss: 0.30034119
    Epoch: 3 	Training Loss: 0.25030788
    Epoch: 3 	Training Loss: 0.24042262
    Epoch: 3 	Training Loss: 0.27047338
    Epoch: 3 	Training Loss: 0.21018668
    Epoch: 3 	Training Loss: 0.31029609
    Epoch: 3 	Training Loss: 0.36035615
    Epoch: 3 	Training Loss: 0.30040468
    Epoch: 3 	Training Loss: 0.33047211
    Epoch: 3 	Training Loss: 0.21020406
    Epoch: 3 	Training Loss: 0.18030651
    Epoch: 3 	Training Loss: 0.30039917
    Epoch: 3 	Training Loss: 0.24050358
    Epoch: 3 	Training Loss: 0.21044897
    Epoch: 3 	Training Loss: 0.24046512
    Epoch: 3 	Training Loss: 0.33059416
    Epoch: 3 	Training Loss: 0.31033341
    Epoch: 3 	Training Loss: 0.36057582
    Epoch: 3 	Training Loss: 0.28054038
    Epoch: 3 	Training Loss: 0.23056369
    Epoch: 3 	Training Loss: 0.25081109
    Epoch: 3 	Training Loss: 0.22041940
    Epoch: 3 	Training Loss: 0.37064823
    Epoch: 3 	Training Loss: 0.21045925
    Epoch: 3 	Training Loss: 0.26035483
    Epoch: 3 	Training Loss: 0.34075573
    Epoch: 3 	Training Loss: 0.24041001
    Epoch: 3 	Training Loss: 0.19035323
    Epoch: 3 	Training Loss: 0.21029766
    Epoch: 3 	Training Loss: 0.30037867
    Epoch: 3 	Training Loss: 0.26029173
    Epoch: 3 	Training Loss: 0.34033310
    Epoch: 3 	Training Loss: 0.29036561
    Epoch: 3 	Training Loss: 0.28031234
    Epoch: 3 	Training Loss: 0.36048888
    Epoch: 3 	Training Loss: 0.20024842
    Epoch: 3 	Training Loss: 0.26044033
    Epoch: 3 	Training Loss: 0.30074577
    Epoch: 3 	Training Loss: 0.23050586
    Epoch: 3 	Training Loss: 0.22058718
    Epoch: 3 	Training Loss: 0.29047564
    Epoch: 3 	Training Loss: 0.33045914
    Epoch: 3 	Training Loss: 0.28074294
    Epoch: 3 	Training Loss: 0.31061235
    Epoch: 3 	Training Loss: 0.31050364
    Epoch: 3 	Training Loss: 0.31039852
    Epoch: 3 	Training Loss: 0.28029721
    Epoch: 3 	Training Loss: 0.28043770
    Epoch: 3 	Training Loss: 0.31044605
    Epoch: 3 	Training Loss: 0.24043245
    Epoch: 3 	Training Loss: 0.30058653
    Epoch: 3 	Training Loss: 0.22064992
    Epoch: 3 	Training Loss: 0.26080071
    Epoch: 3 	Training Loss: 0.24025693
    Epoch: 3 	Training Loss: 0.33043978
    Epoch: 3 	Training Loss: 0.32041897
    Epoch: 3 	Training Loss: 0.38046405
    Epoch: 3 	Training Loss: 0.29047072
    Epoch: 3 	Training Loss: 0.35057674
    Epoch: 3 	Training Loss: 0.26053820
    Epoch: 3 	Training Loss: 0.21047613
    Epoch: 3 	Training Loss: 0.39033749
    Epoch: 3 	Training Loss: 0.25049302
    Epoch: 3 	Training Loss: 0.27035206
    Epoch: 3 	Training Loss: 0.24029357
    Epoch: 3 	Training Loss: 0.25058346
    Epoch: 3 	Training Loss: 0.32044733
    Epoch: 3 	Training Loss: 0.22026752
    Epoch: 3 	Training Loss: 0.24081725
    Epoch: 3 	Training Loss: 0.28022709
    Epoch: 3 	Training Loss: 0.26058564
    Epoch: 3 	Training Loss: 0.23060548
    Epoch: 3 	Training Loss: 0.28054838
    Epoch: 3 	Training Loss: 0.28056532
    Epoch: 3 	Training Loss: 0.32045769
    Epoch: 3 	Training Loss: 0.31074466
    Epoch: 3 	Training Loss: 0.32046035
    Epoch: 3 	Training Loss: 0.28115163
    Epoch: 3 	Training Loss: 0.32075503
    Epoch: 3 	Training Loss: 0.31042888
    Epoch: 3 	Training Loss: 0.24023383
    Epoch: 3 	Training Loss: 0.26024763
    Epoch: 3 	Training Loss: 0.24031644
    Epoch: 3 	Training Loss: 0.26039428
    Epoch: 3 	Training Loss: 0.32056007
    Epoch: 3 	Training Loss: 0.45038927
    Epoch: 3 	Training Loss: 0.25048169
    Epoch: 3 	Training Loss: 0.30067320
    Epoch: 3 	Training Loss: 0.27089258
    Epoch: 3 	Training Loss: 0.30056311
    Epoch: 3 	Training Loss: 0.34069791
    Epoch: 3 	Training Loss: 0.38055083
    Epoch: 3 	Training Loss: 0.26046304
    Epoch: 3 	Training Loss: 0.30054752
    Epoch: 3 	Training Loss: 0.25068576
    Epoch: 3 	Training Loss: 0.25073240
    Epoch: 3 	Training Loss: 0.25049775
    Epoch: 3 	Training Loss: 0.31046754
    Epoch: 3 	Training Loss: 0.32068485
    Epoch: 3 	Validation Loss: 0.76471237
    1:41:47
    Current time: 2019-04-05 04:02:52



```python
plt.plot(losses)
plt.title("Training Loss")
plt.show()
```


![png](output_28_0.png)



```python
# def test(capsule_net, test_loader):
#     '''Prints out test statistics for a given capsule net.
#        param capsule_net: trained capsule network
#        param test_loader: test dataloader
#        return: returns last batch of test image data and corresponding reconstructions
#        '''
#     class_correct = list(0. for i in range(5))
#     class_total = list(0. for i in range(5))
    
#     test_loss = 0 # loss tracking

#     capsule_net.eval() # eval mode

#     for batch_i, (images, target) in enumerate(test_loader):
#         target = torch.eye(5).index_select(dim=0, index=target)

#         batch_size = images.size(0)

#         if TRAIN_ON_GPU:
#             images, target = images.cuda(), target.cuda()

#         # forward pass: compute predicted outputs by passing inputs to the model
#         caps_output, y = capsule_net(images)
#         # calculate the loss
#         loss = criterion(caps_output, target, images)
#         # update average test loss 
#         test_loss += loss.item()
#         # convert output probabilities to predicted class
#         _, pred = torch.max(y.data.cpu(), 1)
#         _, target_shape = torch.max(target.data.cpu(), 1)

#         # compare predictions to true label
#         correct = np.squeeze(pred.eq(target_shape.data.view_as(pred)))
#         # calculate test accuracy for each object class
#         for i in range(batch_size):
#             label = target_shape.data[i]
#             class_correct[label] += correct[i].item()
#             class_total[label] += 1

#     # avg test loss
#     avg_test_loss = test_loss/len(test_loader)
#     print('Test Loss: {:.8f}\n'.format(avg_test_loss))

#     for i in range(5):
#         if class_total[i] > 0:
#             print('Test Accuracy of %5s: %2d%% (%2d/%2d)' % (
#                 str(i), 100 * class_correct[i] / class_total[i],
#                 np.sum(class_correct[i]), np.sum(class_total[i])))
#         else:
#             print('Test Accuracy of %5s: N/A (no training examples)' % (classes[i]))

#     print('\nTest Accuracy (Overall): %2d%% (%2d/%2d)' % (
#         100. * np.sum(class_correct) / np.sum(class_total),
#         np.sum(class_correct), np.sum(class_total)))
    
#     # return last batch of capsule vectors, images, reconstructions
#     return caps_output, images
```


```python

```
