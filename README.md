# capsule-net-dr


##References
***

###Kaggle Dataset
https://www.kaggle.com/c/diabetic-retinopathy-detection

###Original Papers
* https://arxiv.org/pdf/1710.09829.pdf
* https://openreview.net/pdf?id=HJWLfGWRb

###Articles
* https://cezannec.github.io/Capsule_Networks/
* https://pechyonkin.me/capsules-1/

###Implementation
* https://github.com/cezannec/capsule_net_pytorch
* https://github.com/sekwiatkowski/awesome-capsule-networks
* https://github.com/loretoparisi/CapsNet
* https://github.com/XifengGuo/CapsNet-Keras
* https://github.com/acburigo/CapsNet