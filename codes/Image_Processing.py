
# coding: utf-8
#importing the required libraries

import numpy as np
import cv2
from matplotlib import pyplot as plt
import os


#this loads images from the required folder and does histogram equalization on them
def process_images_from_folder(folder):

    for filename in os.listdir(folder):
    	#load images one by one from required folder
        img = cv2.imread(os.path.join(folder,filename))
        #imread returns images in BGR format,so convert it to RGB
        # print(img.shape)
        
        img = cv2.cvtColor(img,cv2.COLOR_BGR2RGB)
        # print(img.shape)
        
        #Now convert the image to Grayscale for Histogram Equilization
        img = cv2.cvtColor(img,cv2.COLOR_RGB2GRAY)
        # print(img.shape)
        #Perform Histogram equilization
        img_eql = cv2.equalizeHist(img)
        # print(img.shape)

        #Convert back to RGB
        # gray = cv2.cvtColor(img_eql,cv2.COLOR_GRAY2RGB)
        #Save the image as per original filename in a new folder
        cv2.imwrite("test/processed/"+str(filename),img_eql)

#Function being called 
process_images_from_folder("test/test")
