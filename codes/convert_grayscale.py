import cv2
from os import listdir
from glob import glob
import numpy as np
from PIL import Image,ImageFilter

training_path='data/train_res'
testing_path='data/test'

# FOR TRAINING
training_classes=listdir(training_path)
for label in training_classes:
    for filename in glob(training_path+'/'+label+'/*.tiff'):
        image = cv2.imread(filename)
#         print(image)
        gray_image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        cv2.imwrite(filename, gray_image)

# # FOR TESTING
# testing_classes=listdir(testing_path)
# for label in testing_classes:
# 	for filename in glob(testing_path+'/'+label+'/*.tiff'):
# 		image = cv2.imread(filename)
# 		gray_image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
# 		cv2.imwrite(filename, gray_image)
