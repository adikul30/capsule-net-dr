### Start Instance

gcloud compute instances start pytorch

### Stop Instance

gcloud compute instances stop pytorch

### SSH

gcloud compute ssh jupyter@pytorch -- -L 8080:localhost:8080

gcloud compute ssh jupyter@tf-instance -- -L 8080:localhost:8080

### Create Instance (To be executed only once when creating the instance)


#### PyTorch Instance

export IMAGE_FAMILY="pytorch-latest-gpu"
export ZONE="us-west1-b"
export INSTANCE_NAME="pytorch"
export INSTANCE_TYPE="n1-highmem-8"

gcloud compute instances create $INSTANCE_NAME \
        --zone=$ZONE \
        --image-family=$IMAGE_FAMILY \
        --image-project=deeplearning-platform-release \
        --maintenance-policy=TERMINATE \
        --accelerator="type=nvidia-tesla-t4,count=2" \
        --machine-type=$INSTANCE_TYPE \
        --boot-disk-size=200GB \
        --metadata="install-nvidia-driver=True" \
        --preemptible

#### Tf Instance

export IMAGE_FAMILY="tf-latest-gpu"
export ZONE="us-west1-b"
export INSTANCE_NAME="tf-instance"
export INSTANCE_TYPE="n1-highmem-8"

gcloud compute instances create $INSTANCE_NAME --zone=$ZONE --image-family=$IMAGE_FAMILY --image-project=deeplearning-platform-release --maintenance-policy=TERMINATE --accelerator="type=nvidia-tesla-t4,count=2" --machine-type=$INSTANCE_TYPE --boot-disk-size=200GB --metadata="install-nvidia-driver=True" --preemptible

### Upload files to bucket from harddisk

gsutil -m cp folder_train_cropped_color.zip gs://dr-dataset/

### Copy files from bucket to instance

gsutil -m cp -r gs://dr-dataset/resized_train .

### Sync dir with bucket

gsutil -m rsync -r dr-dataset/resized_train gs://dr-dataset/resized_train
https://cloud.google.com/storage/docs/gsutil/commands/rsync

### Move n files from train to test

Replace 5 with number of images to move. 
Total number of images in the train and test each should be divisible by 12. 
Otherwise delete the necessary number of images.

cd dr-dataset/train/

find 0/ -maxdepth 1 -type f |head -1028 | xargs -I {} mv {} ../test/0/
find 1/ -maxdepth 1 -type f |head -408 | xargs -I {} mv {} ../test/1/
find 2/ -maxdepth 1 -type f |head -1024 | xargs -I {} mv {} ../test/2/
find 3/ -maxdepth 1 -type f |head -180 | xargs -I {} mv {} ../test/3/
find 4/ -maxdepth 1 -type f |head -180 | xargs -I {} mv {} ../test/4/

### Count files in folder

find train -type f | wc -l
find test -type f | wc -l

### Validation set calculations

    1. Move specified number of images from test to train. 
    2. Delete 2 images from train
    
TOTAL_IMAGES = 35126
TRAIN = 32304
VALIDATION = 2820 (0 = 1028, 1 = 408, 2 = 1024, 3 = 180, 4 = 180 each)

### Delete n random files from a folder

find 0/ -maxdepth 1 -type f -print0 | \
        head -z -n 4 | xargs -0 rm -f {}